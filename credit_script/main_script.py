from enum import Enum
from typing import Union


class BaseEnum(Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class SourceOfIncome(BaseEnum):
    Passive = "Пассивный доход"
    Unemployed = "Безработный"
    OwnBusiness = "Собственный бизнесс"
    HiredWorker = "Наёмный работник"


class Gender(BaseEnum):
    Male = "M"
    Female = "F"


class CreditGoal(BaseEnum):
    Mortgage = "Ипотека"
    BusinessDevelopment = "Развитие бизнеса"
    CarLoan = "Автокредит"
    Consumer = "Потребительский"


class CreditRating(BaseEnum):
    VeryBad = -2
    Bad = -1
    Fair = 0
    Good = 1
    Excellent = 2


class LoanForPerson:
    resolved_modifier = 0

    def __init__(self, name: str = None, age: int = None, gender: Union[Gender, int] = None,
                 source_of_income: Union[SourceOfIncome, str] = None,
                 year_income: int = None, credit_rating: Union[CreditRating, int] = None,
                 requested_amount: Union[float, int] = None,
                 loan_maturity: int = None, goal: Union[CreditGoal] = None):
        self.name = name
        self.age = age
        self.gender = gender
        self.source_of_income = source_of_income
        self.year_income = year_income * 10**6
        self.credit_rating = credit_rating
        self.requested_amount = requested_amount * 10**6
        self.loan_maturity = loan_maturity
        self.goal = goal

    def check_the_age(self) -> bool:
        """
        Check if the loan is available in accordance with the pension age

        :return: False if not available
                 True if available
        """
        loan_maturity_age = self.age + self.loan_maturity
        return False if self.gender == Gender.Female and loan_maturity_age > 60 or\
                        self.gender == Gender.Male and loan_maturity_age > 65 else True

    def check_loan_rating(self) -> bool:
        """
        Check if the loan is available in accordance with the loan rating (-2 in not available)

        :return: False if not available
                 True if available
        """
        return False if self.credit_rating == CreditRating.VeryBad else True

    def check_job_status(self):
        """
        Check if the loan is available in accordance with the source of income (Unemployed is not available)

        :return: False if not available
                 True if available
        """
        return False if self.source_of_income == SourceOfIncome.Unemployed else True

    def check_year_income(self, annual_payment: float, percent: float):
        """
        Check if the loan is available in accordance with the year income

        :param percent: base + modifier
        :param annual_payment: annual payment
        :return: False if not available
                 True if available
        """
        return False if annual_payment+((percent*annual_payment)/100) > (self.year_income/2) else True

    def check_requested_amount(self):
        """
        Check if the loan is available in accordance with the requested amount, loan maturity and year income

        :return: False if not available
                 True if available
        """

        return False if (self.requested_amount/self.loan_maturity) > (self.year_income/3) else True

    def get_credit_sum(self):
        """
        Get loan sum in accordance with the source of income

        :return: False if not available
                 requested_amount if requested_amount is available (< max loan sum)
        """
        resolved_sum_of_credit = []
        if self.source_of_income == SourceOfIncome.Passive:
            max_sum_of_credit = 10**7
            resolved_sum_of_credit.append(max_sum_of_credit)
        elif self.credit_rating == CreditRating.Bad:
            max_sum_of_credit = 10**6
            resolved_sum_of_credit.append(max_sum_of_credit)
        elif self.credit_rating == CreditRating.Fair:
            max_sum_of_credit = 5*(10**6)
            resolved_sum_of_credit.append(max_sum_of_credit)
        elif self.credit_rating == CreditRating.Excellent or self.credit_rating == CreditRating.Good:
            max_sum_of_credit = 10**7
            resolved_sum_of_credit.append(max_sum_of_credit)
        if len(resolved_sum_of_credit) >= 1:
            max_credit = min(resolved_sum_of_credit)
            if self.requested_amount <= max_credit:
                return self.requested_amount
        else:
            return False

    def update_modifier_credit_goal(self) -> None:
        """
        Update modifier in accordance with the loan goal.
        Function updates global entity - self.resolved_modifier

        :return: None
        """
        if self.goal == CreditGoal.Mortgage:
            self.resolved_modifier += 2
        if self.goal == CreditGoal.Consumer:
            self.resolved_modifier += 1.5
        if self.goal == CreditGoal.BusinessDevelopment:
            self.resolved_modifier += 0.5

    def update_modifier_credit_rating(self) -> None:
        """
        Update modifier in accordance with  the loan rating.
        Function updates global entity - self.resolved_modifier

        :return: None
        """
        if self.credit_rating == CreditRating.Bad:
            self.resolved_modifier += 1.5
        if self.credit_rating == CreditRating.Good:
            self.resolved_modifier -= 0.25
        if self.credit_rating == CreditRating.Excellent:
            self.resolved_modifier -= 0.75

    def update_modifier_source_of_income(self):
        """
        Update modifier in accordance with the source_of_income.
        Function updates global entity - self.resolved_modifier

        :return: None
        """
        if self.source_of_income == SourceOfIncome.Passive:
            self.resolved_modifier += 0.5
        if self.source_of_income == SourceOfIncome.HiredWorker:
            self.resolved_modifier -= 0.25
        if self.resolved_modifier == SourceOfIncome.OwnBusiness:
            self.resolved_modifier += 0.25

    @staticmethod
    def _log(requested_sum: float) -> float:
        """
        Get modifier in accordance with the requested sum

        :param requested_sum: requested sum
        :return:
        """
        try:
            if requested_sum / 1000000 == 1:
                return 0
            if requested_sum / 1000000 > 1:
                return requested_sum / 10000000
            if requested_sum / 1000000 < 1:
                return -100000 / requested_sum
        except ZeroDivisionError:
            print("Запрашивая сумма кредита должна быть больше нуля")

    def update_modifier_requested_amount(self):
        """
        Get modifier in accordance with the requested  sum
        Function updates global entity - self.resolved_modifier

        :return: None
        """
        self.resolved_modifier += -self._log(self.requested_amount)

    def modifier(self):
        self.update_modifier_credit_goal()
        self.update_modifier_credit_rating()
        self.update_modifier_requested_amount()
        self.update_modifier_source_of_income()
        return self.resolved_modifier

    def if_credit_available(self):
        """
        Check if loan in available for user

        :return: None if loan in not available for user
                 annual_payment in loan is available
        """
        credit_available = False
        annual_payment = None
        if all([self.check_the_age(), self.check_loan_rating(), self.check_requested_amount(),
                self.check_job_status(), self.get_credit_sum()]):
            credit_available = True
            percent = (10 + self.modifier())
            annual_payment = (self.get_credit_sum()*(1+self.loan_maturity*(percent/100)))/self.loan_maturity
            if not self.check_year_income(annual_payment=annual_payment, percent=percent):
                credit_available = False
        if credit_available:
            print(f"Кредит выдается. Годовой платеж по кредиту: {round(annual_payment)}")
            return round(annual_payment)
        else:
            print(f"Кредит не выдается")
            return None
