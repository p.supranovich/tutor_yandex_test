import pytest

from credit_script.main_script import LoanForPerson, CreditGoal, Gender, CreditRating, SourceOfIncome


class Tests:

    @staticmethod
    def _prepare_person_default_model(
            name="John", age=18, gender: Gender = Gender.Male,
            source_of_income: SourceOfIncome = SourceOfIncome.OwnBusiness,
            year_income=100, credit_rating: CreditRating = CreditRating.Good, requested_amount=0.6,
            loan_maturity=5, goal: CreditGoal = CreditGoal.CarLoan):
        user = LoanForPerson(name=name, age=age, gender=gender, source_of_income=source_of_income,
                             year_income=year_income, credit_rating=credit_rating, requested_amount=requested_amount,
                             loan_maturity=loan_maturity, goal=goal)
        return user.if_credit_available()

    @pytest.mark.parametrize("credit_rating, expected_result",
                             [(CreditRating.Good, 179500),
                              (CreditRating.Excellent, 176500),
                              (CreditRating.Bad,  190000),
                              (CreditRating.VeryBad,  None),
                              (CreditRating.Fair,  181000),
                              ])
    def test_check_credit_rating(self, credit_rating, expected_result):
        actual_result = self._prepare_person_default_model(credit_rating=credit_rating)
        assert actual_result == expected_result

    @pytest.mark.parametrize("age, gender, loan_maturity, expected_result",
                             [(18, Gender.Male, 5,  179500),
                              (18, Gender.Female, 5, 179500),
                              (60, Gender.Male, 5, 179500),
                              (55, Gender.Female, 5, 179500),
                              (65, Gender.Male, 1, None),
                              (60, Gender.Female, 1, None),
                              ])
    def test_check_customer_gender_age_value(self, age, gender, loan_maturity, expected_result):
        actual_result = self._prepare_person_default_model(age=age, gender=gender, loan_maturity=loan_maturity)
        assert actual_result == expected_result
